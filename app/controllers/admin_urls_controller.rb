class AdminUrlsController < ApplicationController
	skip_before_action :verify_authenticity_token
	before_action      :validar_api_key, except: [:redirect_sitio,:obtener_todos_links, :crear_short_url]

	def obtener_short_urls
		usuario = Usuario.where(:email=>params[:email])
		@admin_urls = AdminUrl.where(:usuario=>usuario[0]['id']).order("created_at DESC")
		@admin_urls = @admin_urls.page(params[:pagina] || 1).per(5)
		set_pagination_headers
		render json: @admin_urls , status: :ok
	end

	def dashboard
		sqlRecurrentVisitors = "SELECT ip,COUNT(*) AS total
								FROM uso_urls 
								GROUP BY ip HAVING total > 5 ORDER BY total DESC"
		@recurrentVisitors= ActiveRecord::Base.connection.exec_query(sqlRecurrentVisitors)


		render json: '{"visitantesRecurrentes":'+@recurrentVisitors.to_json+'}' , status: :ok
	end

	def obtener_info_short_url
		@url = AdminUrl.find(params[:id])
		urlTotal = UsoUrl.where(:admin_url =>params[:id])


		sqlUsoUrl = "SELECT user_agent,COUNT(*) AS total
							FROM uso_urls
							WHERE uso_urls.admin_url ="+ params[:id] + " GROUP BY user_agent 
							ORDER BY total DESC"
							
		resultadoSqlUsoUrl= ActiveRecord::Base.connection.exec_query(sqlUsoUrl)

		#urlStatsArray = Array.new
		urlStats=""
		total=0
		resultadoSqlUsoUrl.each do |rSUU|
			user_agent = UserAgentParser.parse rSUU['user_agent']
			total = total + rSUU['total']
			urlStats += '{"dispositivo":"'+user_agent.device.to_s+'","user_agent":"'+user_agent.to_s+'","total":"'+rSUU['total'].to_s+'","porcentaje":"'+((rSUU['total'].to_f/urlTotal.count.to_f)*100).to_s+'"},'
			#urlStatsArray.push(urlStats)
			
		end

		urlStats = urlStats[0...-1]
		if(@url.id > 0)
			render json: '{"dispositivos":['+urlStats+'],"estatus":"OK","url":"'+@url.url+'","url_short":"'+@url.url_short+'","created_at":"'+@url.created_at.to_s+'","clicks":"'+total.to_s+'"}' , status: :ok
		end
	end

	def remove_short_url
		url = AdminUrl.find(params[:short_url])
		if url.destroy
          render json: '{"estatus":"OK"}', status: :no_content
        else
          render json: '{"estatus":"NOK"}', status: :bad_request
        end
	end

	def redirect_sitio
		url = AdminUrl.where(:url_short=>params[:short_url])
		uso_url = UsoUrl.new
		uso_url.admin_url = url[0]['id']
		uso_url.ip = request.remote_ip
		uso_url.user_agent = request.user_agent
		uso_url.save
		redirect_to url[0]['url']
	end

	def create_short_url
		usuario = Usuario.where(:email=>params[:email])
		admin_url = AdminUrl.new
		admin_url.usuario =  usuario[0]['id']
		admin_url.url = params[:url]
		admin_url.save

		url_short = AdminUrl.generate_short_url(admin_url.id,params[:email])
		admin_url.url_short = url_short
		admin_url.save
        if admin_url.save
          render json: '{"estatus":"OK"}', status: :created
        else
          render json: '{"estatus":"NOK"}', status: :bad_request
        end
	end

	#API REST
	def obtener_todos_links
		api_key = request.headers[:apikey]
		usuario = Usuario.where(:apikey=>api_key)
		if(usuario.count() > 0)
			@admin_urls = AdminUrl.where(:usuario=>usuario[0]['id']).order("created_at DESC")
			@admin_urls = @admin_urls.page(params[:pagina] || 1).per(2)
			set_pagination_headers
			render json: @admin_urls , status: :ok
		else
			render json: '{"estatus":"NOK","reason":"API KEY is wrong"}', status: :unauthorized
		end
	end

	#Paginacion
	 def set_pagination_headers
      pc = @admin_urls
      headers["X-Total-Count"] = pc.total_count

      links = []
      links << page_link(1, "first") unless pc.first_page?
      links << page_link(pc.prev_page, "prev") if pc.prev_page
      links << page_link(pc.next_page, "next") if pc.next_page
      links << page_link(pc.total_pages, "last") unless pc.last_page?
      headers["Link"] = links.join(", ") if links.present?
    end

    def page_link(page, rel)
      # "<#{posts_url(request.query_parameters.merge(page: page))}>; rel='#{rel}'"
      base_uri = request.url.split("?").first
      "<#{base_uri}?#{request.query_parameters.merge(page: page).to_param}>; rel='#{rel}'"
    end 

	def crear_short_url
		api_key = request.headers[:apikey]
		usuario = Usuario.where(:apikey=>api_key)
		if(usuario.count() > 0)
			admin_url = AdminUrl.new
			admin_url.usuario =  usuario[0]['id']
			admin_url.url = params[:url]
			if admin_url.save
				url_short = AdminUrl.generate_short_url(admin_url.id,usuario[0]['email'])
				admin_url.url_short = url_short
				admin_url.save
				render json: '{"estatus":"OK","short_url":"http://127.0.0.1:3000/shortUrl/'+url_short+'"}', status: :created
	        else
	          render json: '{"estatus":"NOK","descripcion":"error en el envío de parametros"}', status: :bad_request
	        end
	    else
	    	render json: '{"estatus":"NOK","reason":"API KEY is wrong"}', status: :unauthorized
		end
	end

end
