class ApplicationController < ActionController::Base
	
	def current_usuario
    	return unless session[:usuario_id]
    	@current_usuario ||= Usuario.find(session[:usuario_id])
  	end

	def usuario_autenticado

		if current_usuario == nil
			redirect_to login_path
		end
	end

	def validar_api_key()
		api_key = request.headers[:apikey]
		if api_key.nil?
			render json: '{"estatus":"NOK","reason":"API KEY not found"}', status: :unauthorized
		else
			usuario = Usuario.where(:email=>params[:email])
			if(usuario[0]['apikey']!=api_key)
				render json: '{"estatus":"NOK","reason":"API KEY is wrong"}', status: :unauthorized
			end
		end
	end

	#Paginacion
	 def set_pagination_headers(v_name)
      pc = instance_variable_get("@#{v_name}")
      
      headers["X-Total-Count"] = pc.total_count

      links = []
      links << page_link(1, "first") unless pc.first_page?
      links << page_link(pc.prev_page, "prev") if pc.prev_page
      links << page_link(pc.next_page, "next") if pc.next_page
      links << page_link(pc.total_pages, "last") unless pc.last_page?
      headers["Link"] = links.join(", ") if links.present?
    end

    def page_link(page, rel)
      # "<#{posts_url(request.query_parameters.merge(page: page))}>; rel='#{rel}'"
      base_uri = request.url.split("?").first
      "<#{base_uri}?#{request.query_parameters.merge(page: page).to_param}>; rel='#{rel}'"
    end    

end
