class UsuariosController < ApplicationController
  skip_before_action :verify_authenticity_token

  before_action :usuario_autenticado,:except => [:login,:validar_sesion,:registro,:salvar_registro]

  def login
  end

  def index
  end

  def registro
  end

  def salvar_registro

    if Usuario.exists?(email: params[:email])
      render json: "{estatus:'NOK'}", status: :precondition_failed
    else
        usuario = Usuario.new
        usuario.nombre = params[:nombre]
        usuario.email = params[:email]
        usuario.password = params[:password]
        usuario.apikey = SecureRandom.base58(24)
        usuario.save
        session[:usuario_id] = usuario.usuario
        #respond_to do |format|
          #format.html {redirect_to root_path, info: 'Registro correcto' }
        #end
        if usuario.save
          render json: "{estatus:'OK'}", status: :created
        else
          render json: "{estatus:'NOK'}", status: :bad_request
        end
    end


  end

  def validar_sesion
  	usuario = Usuario.where(:email=>params[:email],:password=>params[:password])
  	if(usuario.count()==1)
  		session[:usuario_id] = usuario[0]['id']
  		#respond_to do |format|
      #		format.html {redirect_to root_path, info: 'Login correcto' }
    	#end
      render json: '{"estatus":"OK","email":"'+usuario[0]['email']+'","nombre":"'+usuario[0]['nombre']+'","apikey":"'+usuario[0]['apikey']+'"}', status: :ok
  	else
  		session[:usuario_id] = nil
        render json: "{estatus:'NOK'}", status: :not_found
  	end
  end

end
