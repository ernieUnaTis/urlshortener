class AdminUrl < ApplicationRecord

	validates_uniqueness_of :url_short

	validates :usuario, presence: true
	validates :url , presence: true

	MAPA = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"


	def self.generate_short_url(id,email)
		short_url = ""
		while id > 0  do 
			short_url += MAPA[id % 62]
			id = id/62
		end
		if(short_url.length < 6)
			short_url = short_url.to_s + Digest::SHA1.hexdigest(email)[8..16].to_s
		end
		return short_url
	end



end
