class Usuario < ApplicationRecord

	validates :email, uniqueness: true
	validates :nombre , presence: true
	validates :password, presence: true

	has_many :admin_urls

end
