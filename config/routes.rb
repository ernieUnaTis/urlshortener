Rails.application.routes.draw do

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: "usuarios#index"

  #Rutas de Usuario Controller
  get 'usuarios/login',as: "login"
  get 'usuarios/registro',as: "registro"
  post 'usuarios/validar_sesion',as: "validar_sesion"
  post 'usuarios/salvar_registro',as: "salvar_registro"

  resources :admin_urls

  #UrlShort
  post 'dashboard',to: "admin_urls#dashboard"
  post 'obtener_short_urls',to: "admin_urls#obtener_short_urls"
  post 'create_short_url',to: "admin_urls#create_short_url"
  post 'almacenar_datos_request',to: "admin_urls#almacenar_datos_request" 
  post 'remove_short_url', to: "admin_urls#remove_short_url"
  post 'obtener_info_short_url',to: "admin_urls#obtener_info_short_url" 
  get 'shortUrl/:short_url',to: "admin_urls#redirect_sitio" 

  #REST_API_KEY
  get 'obtener_todos_links',to: "admin_urls#obtener_todos_links"
  post 'crear_short_url',to: "admin_urls#crear_short_url"

end
