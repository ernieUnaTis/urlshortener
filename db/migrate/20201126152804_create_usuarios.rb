class CreateUsuarios < ActiveRecord::Migration[6.0]
  def change
    create_table :usuarios do |t|
      t.integer :usuario
      t.string :nombre
      t.string :email
      t.string :password

      t.timestamps
    end
  end
end
