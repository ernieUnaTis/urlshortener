class CreateAdminUrls < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_urls do |t|
      t.integer :usuario
      t.string  :url
      t.string  :url_short
      t.timestamps
    end
  end
end
