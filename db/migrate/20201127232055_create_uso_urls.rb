class CreateUsoUrls < ActiveRecord::Migration[6.0]
  def change
    create_table :uso_urls do |t|
      t.integer :admin_url
      t.string :ip
      t.string :user_agent
      t.string :os

      t.timestamps
    end
  end
end
