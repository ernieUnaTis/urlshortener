require 'rails_helper'

RSpec.describe 'AdminUrls', type: :request do

	describe 'get obtener_short_urls' do
	  	it 'returns success' do
	  		post '/obtener_short_urls',params: {email: "edut83@gmail.com" },headers: { "apikey" => "12345" }
			expect(response).to have_http_status(:ok)
	    end
  	end

  	describe 'get obtener_short_urls 401' do
	  	it 'returns success' do
	  		post '/obtener_short_urls',params: {email: "edut83@gmail.com" },headers: { "apikey" => "1235" }
			expect(response).to have_http_status(:unauthorized)
	    end
  	end

  	describe 'get obtener_info_short_url 200' do
	  	it 'returns success' do
	  		post '/obtener_info_short_url',params: {email: "edut83@gmail.com",id: 1 },headers: { "apikey" => "12345" }
			expect(response).to have_http_status(:ok)
	    end
  	end

  	describe 'remove obtener_info_short_url 200' do
	  	it 'returns success' do
	  		post '/obtener_info_short_url',params: {id: 11,email: "edut83@gmail.com"},headers: { "apikey" => "12345" }
			expect(response).to have_http_status(:not_content)
	    end
  	end



end
