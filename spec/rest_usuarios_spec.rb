require 'rails_helper'

RSpec.describe 'Usuarios', type: :request do
  
  describe 'POST login' do
  	it 'returns success' do
    	post '/usuarios/validar_sesion',params: {email: "edut83@gmail.com", password: "123456" }
		expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST Error login' do
  	it 'returns success' do
    	post '/usuarios/validar_sesion',params: {email: "edut85@gmail.com", password: "123456" }
		expect(response).to have_http_status(:not_found)
    end
  end

  describe 'Crear Usuario correcto' do
  	it 'returns success' do
    	post '/usuarios/salvar_registro',params: {email: "danny12@gmail.com", password: "123456",nombre:"Daniela"}
		expect(response).to have_http_status(:created)
    end
  end

   describe 'Crear Usuario correcto' do
  	it 'returns success' do
    	post '/usuarios/salvar_registro',params: {email: "edut83@gmail.com", password: "123456",nombre:"Daniela"}
		expect(response).to have_http_status(:precondition_failed)
    end
  end

  describe 'Error Crear Usuario correcto'  do
  	it 'returns success' do
    	post '/usuarios/validar_sesion',params: {email: "", password: "123456",nombre:"" }
		expect(response).to have_http_status(:not_found)
    end
  end

end