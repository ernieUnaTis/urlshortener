require 'test_helper'

class UsuariosControllerTest < ActionDispatch::IntegrationTest
  test "should get login" do
    get usuarios_login_url
    assert_response :success
  end

  test "should get index" do
    get usuarios_index_url
    assert_response :success
  end

end
